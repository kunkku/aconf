# Copyright (c) 2012-2020 Kaarle Ritvanen
# See LICENSE file for license details

ROOT_DIR := /
LUA_VERSION := 5.3

confdir := etc/aconf
luadir := usr/share/lua/$(LUA_VERSION)/aconf
resdir := usr/share/aconf
augdir := $(resdir)/augeas
webdir := $(resdir)/web
docdir := usr/share/doc/aconf
starter := usr/sbin/aconfd

files := $(starter)


all: doc/index.html

doc/%: aconf/model/config.ld $(shell find aconf -name '*.lua')
	ldoc -f markdown -M -p aconf aconf

define _copy =
	install -d $$(dir $$@)
	install -m $(if $(1),$(1),644) $$< $$@
endef

define copy =
$(ROOT_DIR)/$(2)/$(3): $(1)/$(3)
$(call _copy,$(4))

files += $(2)/$(3)
endef

define rcopy =
$(ROOT_DIR)/$(2)/%.$(3): $(1)/%.$(3)
$(call _copy)

files += $(patsubst $(1)/%.$(3),$(2)/%.$(3),$(shell find $(1) -name '*.$(3)' $(4)))
endef

define link =
$(ROOT_DIR)/$(resdir)/$(1):
	install -d $$(dir $$@)
	ln -s /$(2) $$@

files += $(resdir)/$(1)
endef


$(eval $(call link,aconf,$(luadir)))
$(eval $(call rcopy,aconf,$(luadir),lua,-not -path 'aconf/modules/demo-*'))

$(eval $(call rcopy,augeas,$(augdir),aug))

$(eval $(call link,config,$(confdir)))
$(eval $(call copy,config,$(confdir),audit.json,600))
$(eval $(call copy,config,$(confdir),auth.json,600))

$(eval $(call copy,doc,$(docdir),index.html))
$(eval $(call copy,doc,$(docdir),ldoc.css))

$(eval $(call copy,.,$(resdir),LICENSE))

$(ROOT_DIR)/$(starter): run-server.sh
	install -d $(dir $@)
	install -m 755 $< $@

$(eval $(call copy,.,$(resdir),server.lua))

$(eval $(call rcopy,web,$(webdir),css))
$(eval $(call rcopy,web,$(webdir),html))
$(eval $(call rcopy,web,$(webdir),js))


install: $(foreach f,$(files),$(ROOT_DIR)/$(f))

.PHONY: all install

.NOTPARALLEL:
