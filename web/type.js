/*
 * Copyright (c) 2012-2015 Kaarle Ritvanen
 * See LICENSE file for license details
 */

angular.module("aconf").factory("aconfType", function() {
    function isList(meta) {
	return _.contains(["list", "set"], meta.type);
    }

    function isCollection(meta) {
	return meta.type == "collection" || isList(meta);
    }

    return {
	isList: isList,
	isCollection: isCollection,
	isTreeNode: function(meta) {
	    return meta.type == "model" || isCollection(meta);
	}
    };
});
