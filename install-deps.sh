#!/bin/sh

# Copyright (c) 2012-2018 Kaarle Ritvanen
# See LICENSE file for license details

PACKAGES="lua5.3-augeas lua5.3-b64 lua5.3-cjson lua5.3-file-magic lua5.3-openrc
          lua5.3-ossl lua5.3-posix lua5.3-stringy uwsgi uwsgi-lua"

[ "$1" = -d ] && PACKAGES="$PACKAGES bash curl ldoc"

exec apk add --virtual .aconf-deps $PACKAGES
