--[[
Copyright (c) 2012-2016 Kaarle Ritvanen
See LICENSE file for license details
--]]

--- @module aconf.model
local M = {}

--- create a new class.
-- @function object.class
-- @param base (optional <i>**&lt;Class&gt;**</i>) base class
-- inherited by the new class.
-- @return <i>**&lt;Class&gt;**</i> new class
function M.class(base)
   local cls = {}

   local mt = {
      __call=function(self, ...)
		local obj = {}
		setmetatable(obj, {__index=cls, class=cls})
		obj:init(...)
		return obj
	     end
   }

   if not base and M.Object then base = M.Object end
   if base then
      cls._base = base
      mt.__index = base
   end

   return setmetatable(cls, mt)
end


M.Object = M.class()

function M.Object:init(...) end


function M.super(obj, cls)
   assert(M.isinstance(obj, cls))

   local mt = {}

   function mt.__index(t, k)
      local v = cls._base[k]
      if type(v) ~= 'function' then return v end
      return function(...)
		local arg = {...}
		arg[1] = obj
		return v(table.unpack(arg))
	     end
   end

   return setmetatable({}, mt)
end


function M.issubclass(cls1, cls2)
   if cls1 == cls2 then return true end
   if cls1._base then return M.issubclass(cls1._base, cls2) end
   return false
end

function M.isinstance(obj, cls)
   if not obj or type(obj) ~= 'table' then return false end
   local mt = getmetatable(obj)
   if not mt then return false end
   return type(obj) == 'table' and mt.class and M.issubclass(mt.class, cls)
end

function M.toinstance(obj, ...)
   return getmetatable(obj).class and obj or obj(...)
end


return M
