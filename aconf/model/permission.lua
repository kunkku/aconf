--[[
Copyright (c) 2012-2015 Kaarle Ritvanen
See LICENSE file for license details
--]]

--- @module aconf.model
local M = {}

local node = require('aconf.model.node')
local start_txn = require('aconf.transaction')

function M.define(path, ...)
   local txn = start_txn()
   local db = txn:fetch('/aaa/permissions')
   for _, permission in ipairs{...} do node.insert(db, permission..path) end
   txn:commit()
end

--- create default permissions (read, create, modify, delete) for a given path.
-- @function permission.defaults
-- @tparam string path path to create the permissions for
function M.defaults(path)
   M.define(path, 'read', 'create', 'modify', 'delete')
   for _, action in ipairs(node.meta(start_txn():fetch(path)).actions or {}) do
      M.define(path, action.name)
   end
end

return M
