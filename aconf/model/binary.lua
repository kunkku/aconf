--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

local M = {}

local object = require('aconf.object')
local class = object.class
local super = object.super


local b64 = require('b64')

local magic = require('magic')
magic = magic.open(magic.MIME_TYPE)
magic:load()


M.Data = class()

function M.Data:init(path, data)
   self.path = path
   self.data = data
   self.type = magic:buffer(data)
end

function M.Data:encode()
   return 'data:'..self.type..';base64,'..b64.encode(self.data)
end


M.Audio = class(require('aconf.model.field').Field)

function M.Audio:init(params)
   super(self, M.Audio):init(params)
   self.dtype = 'binary'
   self.widget = 'audio'
end

function M.Audio:load(context)
   local value = super(self, M.Audio):load(context)
   return type(value) == 'string' and M.Data(context.path, value) or value
end

-- not yet implemented
function M.Audio:save(context, value) end


return M
