--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

local M = {}

local err = require('aconf.error')
local raise = err.raise

local fld = require('aconf.model.field')
local String = fld.String

local to_field = require('aconf.model.model').to_field

local object = require('aconf.object')
local class = object.class
local super = object.super


local stringy = require('stringy')


M.Range = class(String)

function M.Range:init(params)
   super(self, M.Range):init(params)
   if not self.type then self.type = fld.Integer end
end

function M.Range:validate(context, value)
   local comps = stringy.split(value, '-')
   if #comps > 2 then raise(context.path, 'Invalid range') end
   for _, v in ipairs(comps) do to_field(self.type):_validate(context, v) end
end


M.Union = class(String)

function M.Union:validate(context, value)
   super(self, M.Union):validate(context, value)
   for _, tpe in ipairs(self.types) do
      local field = to_field(tpe)
      if err.call(field.validate, field, context, value) then return end
   end
   raise(context.path, self.error or 'Invalid value')
end


return M
