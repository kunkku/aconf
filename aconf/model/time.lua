--[[
Copyright (c) 2012-2015 Kaarle Ritvanen
See LICENSE file for license details
--]]

local M = {}

local raise = require('aconf.error').raise

local object = require('aconf.object')
local class = object.class
local super = object.super


local posix = require('posix')


M.Timestamp = class(require('aconf.model.field').String)
M.Date = class(M.Timestamp)
M.Time = class(M.Timestamp)

M.Date.format = '%Y-%m-%d'
M.Time.format = '%H:%M:%S'
M.Timestamp.format = M.Date.format..' '..M.Time.format

function M.Timestamp:_offset(value)
   if not value then return end
   if type(value) == 'number' then return value end
   local time = posix.strptime(value, self.format)
   return time and os.time(time)
end

function M.Timestamp:decode(context, value)
   return value and self.epoch_offset and os.date(self.format, value) or value
end

function M.Timestamp:encode(context, value)
   return self.epoch_offset and self:_offset(value) or value
end

function M.Timestamp:normalize(context, value)
   local time = self:_offset(value)
   return time and os.date(self.format, time) or value
end

function M.Timestamp:validate(context, value)
   super(self, M.Timestamp):validate(context, value)
   if not self:_offset(value) then raise(context.path, 'Invalid value') end
end

function M.Date:init(params)
   super(self, M.Date):init(params)
   self.widget = 'date'
end

function M.Time:init(params)
   super(self, M.Time):init(params)
   if params.seconds == false then self.format = '%H:%M' end
end


return M
