--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

local M = {}

M.model = require('aconf.model')

require('aconf.model.aaa')
local mods = require('aconf.loader')('modules')

M.call = require('aconf.error').call
M.object = require('aconf.object')
M.path = require('aconf.path')
M.start_txn = require('aconf.transaction')

local txn = M.start_txn()
for _, rv in pairs(mods) do if type(rv) == 'function' then rv(txn) end end
txn:commit()

local def_store = require('aconf.persistence.defer')
function M.commit() def_store:commit() end

return M
