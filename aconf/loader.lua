--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

local pth = require('aconf.path')

local posix = require('posix')
local stringy = require('stringy')

return function(subdir)
	  local comps = pth.split('aconf/'..subdir)
	  local res = {}
	  for _, modfile in ipairs(posix.dir(pth.join(table.unpack(comps)))) do
	     if stringy.endswith(modfile, '.lua') then
		local name = modfile:sub(1, -5)
		res[name] = require(table.concat(comps, '.')..'.'..name)
	     end
	  end
	  return res
       end
