--[[
Copyright (c) 2012-2018 Kaarle Ritvanen
See LICENSE file for license details
--]]

local M = {}

function M.open_file(path, mode)
   local file = io.open(path, mode)
   if not file then error('Cannot open file: '..path) end
   return file
end

function M.read_file(path)
   local file = M.open_file(path)
   local data = file:read('a')
   file:close()
   return data
end

function M.tostring(value)
   -- TODO make values configurable per address
   if type(value) == 'boolean' then return value and 'yes' or 'no' end
   return tostring(value)
end

return M
