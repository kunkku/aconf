--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

local util = require('aconf.util')


local backend = require('aconf.object').class()

function backend:init(data) self.data = data or {} end

function backend:_get(path)
   local res = self.data
   for _, comp in ipairs(path) do
      if res == nil then return end
      assert(type(res) == 'table')
      res = res[comp]
   end
   return res
end

function backend:get(path, top)
   local res = self:_get(path)
   return type(res) == 'table' and util.keys(res) or res
end

function backend:_set(path, value)
   if type(value) == 'table' then value = {} end

   if #path == 0 then self.data = value

   else
      local comps = util.copy(path)
      local name = comps[#comps]
      table.remove(comps)
      self:_get(comps)[name] = value
   end
end

function backend:set(mods)
   for _, mod in ipairs(mods) do self:_set(table.unpack(mod)) end
end


return backend
