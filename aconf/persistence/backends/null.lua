--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

local backend = require('aconf.object').class()
function backend:get(path, top) return #path == 0 and {} or nil, 0 end
function backend:set(mods) end

return backend
