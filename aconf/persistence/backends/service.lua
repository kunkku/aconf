--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

local rc = require('rc')
local stringy = require('stringy')


local backend = require('aconf.object').class()

function backend:get(path, top)
   if #path == 1 then return {'enabled', 'status'} end
   assert(#path == 2)
   local status = rc.service_status(path[1])
   if path[2] == 'status' then return status end
   if path[2] == 'enabled' then return stringy.startswith(status, 'start') end
end

function backend:set(mods)
   for _, mod in ipairs(mods) do
      local path, value = table.unpack(mod)
      assert(#path == 2 and path[2] == 'enabled')

      local name = path[1]
      if value then rc.service_add(name)
      else rc.service_delete(name) end
      os.execute('rc-service '..name..' '..(value and 'start' or 'stop'))
   end
end

return backend
