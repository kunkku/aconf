--[[
Copyright (c) 2012-2015 Kaarle Ritvanen
See LICENSE file for license details
--]]

local M = require('aconf.model')

local filter_conf = '/etc/dnsmasq-filter.conf'

local Address = M.new()
Address.address = M.net.IPAddress{be_mode='parent-value'}
Address.domain = M.String

local Filter = M.new()
Filter.enabled = M.Boolean{
   compute=function(obj)
      return M.node.contains(obj:fetch('../conf-file'), filter_conf)
   end,
   store=function(obj, value)
      local files = obj:fetch('../conf-file')
      if value then M.node.insert(files, filter_conf)
      else files[filter_conf] = nil end
   end
}
Filter.redirect_address = M.net.IPAddress{
   compute=function(obj)
      local addr = obj.address
      return addr and addr.address
   end,
   store=function(obj, value)
      obj.address = value and {address=value, domain='#'} or nil
   end
}
Filter.address = M.Model{model=Address, visible=false}
Filter.whitelist = M.Set{
   type=M.net.DomainName,
   addr='server/#/domain',
   ui_name='Domain whitelist',
   widget='inline'
}

local Dnsmasq = M.new()
Dnsmasq.filter = M.Model{
   model=Filter,
   addr='/augeas'..filter_conf,
   be_mode={server='value', ['server/#/domain']='enumerate'}
}
Dnsmasq.conf_file = M.Set{
   type=M.String,
   addr='/augeas/etc/dnsmasq.conf/conf-file',
   be_mode='enumerate',
   visible=false
}

M.register('dnsmasq', Dnsmasq)
M.permission.defaults('/dnsmasq')
