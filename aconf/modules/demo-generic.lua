--[[
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
--]]

-- provided as an example, to be removed from production version

local M = require('aconf.model')

M.register('proc', M.Mixed, {addr='/files/proc', ui_name='/proc'})
M.permission.defaults('/proc')

M.register('augeas', M.Mixed, {addr='/augeas'})
M.permission.defaults('/augeas')
