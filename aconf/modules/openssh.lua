--[[
Copyright (c) 2013 Natanael Copa <ncopa@alpinelinux.org>
Copyright (c) 2013-2015 Kaarle Ritvanen
See LICENSE file for license details
--]]

local M = require('aconf.model')

local SshKey = M.new()
SshKey.type = M.String{
   required=true,
   choice={
      {'ecdsa-sha2-nistp256', 'ecdsa-sha2-nistp256'},
      {'ecdsa-sha2-nistp384', 'ecdsa-sha2-nistp384'},
      {'ecdsa-sha2-nistp521', 'ecdsa-sha2-nistp521'},
      {'ssh-dss', 'ssh-dss'},
      {'ssh-rsa', 'ssh-rsa'}
   }
}
SshKey.key = M.String{be_mode='parent-value'}
SshKey.comment = M.String

local Sshd = M.service('sshd')
Sshd.root_login = M.Boolean{
   addr='PermitRootLogin', ui_name='Permit root login', default=true
}
Sshd.password_auth = M.Boolean{
   addr='PasswordAuthentication',
   ui_name='Password authentication',
   default=true
}
Sshd.use_dns = M.Boolean{addr='UseDNS', ui_name='Use DNS', default=true}
Sshd.agent_forward = M.Boolean{
   addr='AllowAgentForwarding', ui_name='Allow agent forwarding', default=true
}
Sshd.root_auth_keys = M.List{
   type=SshKey,
   addr='/augeas/root/.ssh/authorized_keys/key',
   be_mode='enumerate',
   ui_name='Root login keys',
   ui_member='Key'
}

M.register(
   'sshd',
   Sshd,
   {addr='/augeas/etc/ssh/sshd_config', ui_name='SSH daemon'}
)
M.permission.defaults('/sshd')
