(*
Copyright (c) 2012-2014 Kaarle Ritvanen
See LICENSE file for license details
*)

module AConf_Dnsmasq =

autoload xfm

let filter = incl "/etc/dnsmasq-filter.conf"
let xfm = transform Dnsmasq.lns filter
