#!/bin/sh

# Copyright (c) 2012-2015 Kaarle Ritvanen
# See LICENSE file for license details

export AUGEAS_LENS_LIB=augeas

if [ $(basename $0) = run-server.sh ]; then
    export LUA_PATH="./?.lua;./?/init.lua;;"
    PORT=${1:-8000}
    OPTIONS=
    cd $(dirname $0)
else
    PORT=80
    OPTIONS="-d /var/log/aconf.log --pidfile /var/run/aconf.pid"
    cd /usr/share/aconf
fi

exec uwsgi -M --http-socket :$PORT --close-on-exec2 \
    --plugin /usr/lib/uwsgi/lua_plugin.so --lua server.lua \
    --remap-modifier 6:0 \
    --static-map /browser=web --static-index client.html \
    $OPTIONS
